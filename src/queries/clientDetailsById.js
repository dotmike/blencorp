import { gql } from "@apollo/client";

export default gql`
  query clientDetailsById($id: String) {
    clientDetailsById(id: $id) {
      id
      email
      company
      phone
      address
    }
  }
`;
