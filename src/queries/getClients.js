import { gql } from "@apollo/client";

export default gql`
  query GetClients {
    clients {
      id
      name
      age
    }
  }
`;
