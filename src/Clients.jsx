import React from 'react'
import { useQuery } from '@apollo/client'
import GetClients from './queries/getClients'
import Table from 'react-bootstrap/Table'
import classNames from 'classnames'

export default function Clients(props) {
  const { loading, error, data } = useQuery(GetClients)
  if (loading) return <h3>Loading...</h3>
  if (error) return <h3>Sorry, there was an error.</h3>
  return (
    <section className={classNames('clients', 'pt-5')}>
      <h3>Clients</h3>
      <Table className={classNames('table', 'table-striped')}>
        <thead className="thead-dark">
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody>
          {data.clients.map((client, key) => (
            <tr id={client.id} key={key}>
              <td>{client.id}</td>
              <td>{client.name}</td>
              <td>{client.age}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </section>
  )
}
