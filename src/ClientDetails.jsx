import React from 'react'
import { useQuery } from '@apollo/client'
import clientDetailsById from './queries/clientDetailsById'
import { useParams } from 'react-router-dom'

export default function ClientDetails(props) {
  let { id } = useParams()

  const cid = '59761c233d8d0f92a6b0570d'
  const { loading, error, data } = useQuery(clientDetailsById, {
    variables: cid,
  })

  console.log('data:', data)
  console.log('id:', id)

  return <div>Now showing post {id}</div>

  if (loading) return <h3>Loading...</h3>
  if (error) return <h3>Error: {error.message}</h3>
  return (
    <section className="clientDetails">
      <h5>Client Details</h5>
      {data.clientDetails.map((clientDetails) => (
        <p>
          {clientDetails.company} - {clientDetails.email} -{' '}
          {clientDetails.phone} - {clientDetails.address} - {clientDetails.id}
        </p>
      ))}
    </section>
  )
}
