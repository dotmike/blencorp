const { ApolloServer } = require("apollo-server");
const ClientAPI = require("./datasources/client");
const ClientDetailsAPI = require("./datasources/clientDetails");

require("dotenv").config();
const typeDefs = require("./schema.js");

const resolvers = require("./resolvers.js");

const dataSources = () => ({
  ClientAPI: new ClientAPI(),
  ClientDetailsAPI: new ClientDetailsAPI()
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources,
  debug: false
});

server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
  console.log(`GraphQL running at ${url}`);
});
