const { gql } = require("apollo-server");

module.exports = gql`
  type Query {
    clients(id: String, age: Int, name: String): [Client]
    clientDetails: [ClientDetails]
    clientDetailsById(id: String): ClientDetails
  }
  type ClientDetails {
    id: String!
    gender: String
    company: String
    email: String
    phone: String
    address: String
  }
  type Client {
    id: String!
    age: Int
    name: String
    clientDetails: [ClientDetails]
  }
`;
