const _ = require("lodash");

module.exports = {
  clientDetails: async (client, args, { dataSources }, info) => {
    const clientDetails = await dataSources.ClientDetailsAPI.getClientDetails();
    const returns = clientDetails.filter((clientDetailsById) => {
      return _.filter(client.id, { id: clientDetailsById.id }).length > 0;
    });
    return returns;
  }
};
