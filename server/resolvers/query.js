module.exports = {
  clients: (parent, args, { dataSources }, info) => {
    return dataSources.ClientAPI.getClients(args);
  },
  clientDetails: (parent, args, { dataSources }, info) => {
    return dataSources.ClientDetailsAPI.getClientDetails();
  },
  clientDetailsById: (parent, { id }, { dataSources }, info) => {
    return dataSources.ClientDetailsAPI.getClientDetailsById(id);
  }
};
