const clients = require("../data/clients.json");
const { DataSource } = require("apollo-datasource");
const _ = require("lodash");

class ClientAPI extends DataSource {
  constructor() {
    super();
  }

  initialize(config) {}

  getClients(args) {
    return _.filter(clients, args);
  }
}

module.exports = ClientAPI;
